﻿CREATE TABLE [dbo].[ScriptLog] (
    [ScriptName] VARCHAR (250) NOT NULL,
    [ScriptDate] SMALLDATETIME NOT NULL,
    [RunDate]    DATETIME      NULL,
    [DBVersion]  VARCHAR (50)  NOT NULL,
    [Comments]   VARCHAR (150) NOT NULL
);

