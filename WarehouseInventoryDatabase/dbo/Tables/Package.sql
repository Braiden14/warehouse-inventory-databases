﻿CREATE TABLE [dbo].[Package] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [PackageType] VARCHAR (100) NOT NULL,
    [Weight]      FLOAT (53)    NOT NULL,
    [Length]      FLOAT (53)    NOT NULL,
    [Height]      FLOAT (53)    NOT NULL,
    [Width]       FLOAT (53)    NOT NULL,
    [Quantity]    INT           NOT NULL,
    [Contents]    VARCHAR (100) NULL,
    [IsFragile]   BIT           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

