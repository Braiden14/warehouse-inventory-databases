CREATE PROCEDURE [dbo].[spUpdatePackageType]
	@Id UNIQUEIDENTIFIER,
	@Type VARCHAR(100)
AS
	BEGIN
		UPDATE PackageType
		SET [Type] = @Type
		WHERE Id = @Id
END
