ALTER PROCEDURE [dbo].[spGetPackageById]
@Id UNIQUEIDENTIFIER
AS
BEGIN 
SELECT p.[Id]
      ,p.[PackageTypeId]
      ,p.[Weight]
      ,p.[Length]
      ,p.[Height]
      ,p.[Width]
      ,p.[Quantity]
      ,p.[Contents]
      ,p.[IsFragile]
  FROM [dbo].[Package] p
  LEFT JOIN PackageType pt
  ON p.PackageTypeId = pt.Id
  WHERE p.Id = @Id
  AND p.IsDeleted = 0
END