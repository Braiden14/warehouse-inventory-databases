CREATE PROCEDURE [dbo].[spGetPackageTypeById]
@Id UNIQUEIDENTIFIER
AS
BEGIN 
SELECT [Id]
      ,[Type]
  FROM [dbo].[PackageType]
WHERE Id = @Id
END