ALTER PROCEDURE [dbo].[spDeletePackage]
	@Id UNIQUEIDENTIFIER
AS
	BEGIN
		UPDATE Package
		SET IsDeleted = 1
		WHERE Id = @Id
	END

