ALTER PROCEDURE [dbo].[spGetPackages]
	@Filter varchar(100)
AS
BEGIN
SELECT [Id]
      ,PackageTypeId
      ,[Weight]
      ,[Length]
      ,[Height]
      ,[Width]
      ,[Quantity]
      ,[Contents]
      ,[IsFragile]
	  ,[Width] * [Height] * [Length] AS Volume
	  ,[PackageTypeId]
FROM [dbo].[Package]
WHERE (CHARINDEX(@Filter, Id) > 0 OR CHARINDEX(@Filter, Contents) > 0 OR @Filter IS NULL OR LEN(@Filter) = 0) 
AND IsDeleted = 0
END