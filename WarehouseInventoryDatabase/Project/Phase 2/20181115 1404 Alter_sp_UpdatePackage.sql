ALTER PROCEDURE [dbo].[spUpdatePackage]
	@Id UNIQUEIDENTIFIER,
	@PackageTypeId VARCHAR(100),
	@Weight FLOAT,
	@Length FLOAT,
	@Height FLOAT,
	@Width FLOAT,
	@Quantity INT,
	@Contents VARCHAR(100),
	@IsFragile BIT
AS
	BEGIN
		UPDATE Package
		SET [PackageTypeId] = @PackageTypeId,
			[Weight] = @Weight,
			[Length] = @Length,
			[Height] = @Height,
			[Width] = @Width,
			[Quantity] = @Quantity, 
			[Contents] = @Contents,
			[IsFragile] = @IsFragile
		WHERE Id = @Id
END
