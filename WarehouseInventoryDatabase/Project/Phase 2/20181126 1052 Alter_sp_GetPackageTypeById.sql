ALTER PROCEDURE [dbo].[spGetPackageTypeById]
@Id UNIQUEIDENTIFIER
AS
BEGIN 
SELECT [Id]
      ,[Type]
  FROM [dbo].[PackageType]
WHERE Id = @Id 
AND IsDeleted = 0
END