ALTER PROCEDURE [dbo].[spGetPackages]
	@Filter varchar(100)
AS
BEGIN
SELECT p.[Id]
      ,pt.[Type] AS PackageType
      ,p.[Weight]
      ,p.[Length]
      ,p.[Height]
      ,p.[Width]
      ,p.[Quantity]
      ,p.[Contents]
      ,p.[IsFragile]
FROM [dbo].[Package] p
LEFT JOIN PackageType pt
ON p.PackageTypeId = pt.Id
WHERE (CHARINDEX(@Filter, p.Id) > 0 OR CHARINDEX(@Filter, pt.[Type]) > 0 OR CHARINDEX(@Filter, p.Contents) > 0 OR @Filter IS NULL OR LEN(@Filter) = 0) 
AND p.IsDeleted = 0
END