ALTER PROCEDURE [dbo].[spGetPackages]
	@Filter varchar(100)
AS
BEGIN
SELECT [p].[Id]
      ,[p].[PackageTypeId]
      ,[p].[Weight]
      ,[p].[Length]
      ,[p].[Height]
      ,[p].[Width]
      ,[p].[Quantity]
      ,[p].[Contents]
      ,[p].[IsFragile]
	  ,[p].[Width] * [Height] * [Length] AS Volume
	  ,[p].[PackageTypeId]
	  ,[pt].[Type]
FROM [dbo].[Package] p LEFT JOIN [dbo].[PackageType] pt
ON p.PackageTypeId = pt.Id
WHERE (CHARINDEX(@Filter, [p].[Id]) > 0 OR CHARINDEX(@Filter, [Contents]) > 0 OR CHARINDEX(@Filter, [Type]) > 0 OR @Filter IS NULL OR LEN(@Filter) = 0) 
AND [p].[IsDeleted] = 0 
AND [pt].[IsDeleted] = 0 OR [pt].[IsDeleted] IS NULL
END
