CREATE TABLE [dbo].[PackageType](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY,
	[Type] [varchar](100) NOT NULL,
	[IsDeleted] BIT DEFAULT 0
	);