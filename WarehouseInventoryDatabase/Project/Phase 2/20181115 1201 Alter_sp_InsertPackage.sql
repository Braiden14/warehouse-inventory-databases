ALTER PROCEDURE [dbo].[spInsertPackage]
    @Id UNIQUEIDENTIFIER,
	@PackageTypeId VARCHAR(100),
	@Weight FLOAT,
	@Length FLOAT,
	@Height FLOAT,
	@Width FLOAT,
	@Quantity INT,
	@Contents VARCHAR(100),
	@IsFragile BIT
AS
	BEGIN
		INSERT INTO Package 
		Values(@Id, @Weight, @Length, @Height, @Width, @Quantity, @Contents, @IsFragile, @PackageTypeId, 0)
	END

