CREATE PROCEDURE [dbo].[spDeletePackageType]
	@Id UNIQUEIDENTIFIER
AS
	BEGIN
		UPDATE PackageType SET IsDeleted = 1
	END