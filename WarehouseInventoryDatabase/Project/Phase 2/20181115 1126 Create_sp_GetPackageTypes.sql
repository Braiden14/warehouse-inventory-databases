CREATE PROCEDURE [dbo].[spGetPackageTypes]
	@Filter varchar(100)
AS
BEGIN
SELECT [Id]
      ,[Type]     
FROM [dbo].[PackageType]
WHERE (CHARINDEX(@Filter, Id) > 0 OR CHARINDEX(@Filter, [Type]) > 0)
END