﻿CREATE TABLE [dbo].[Package](
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[PackageType] [varchar](100) NOT NULL,
	[Weight] [float] NOT NULL,
	[Length] [float] NOT NULL,
	[Height] [float] NOT NULL,
	[Width] [float] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Contents] [varchar](100) NULL,
	[IsFragile] [bit] NULL
	);

