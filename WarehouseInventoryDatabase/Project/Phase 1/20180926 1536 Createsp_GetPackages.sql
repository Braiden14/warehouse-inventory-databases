﻿CREATE PROCEDURE [dbo].[spGetPackages]
	@Filter varchar(100)
AS
BEGIN
SELECT [Id]
      ,[PackageType]
      ,[Weight]
      ,[Length]
      ,[Height]
      ,[Width]
      ,[Quantity]
      ,[Contents]
      ,[IsFragile]
FROM [dbo].[Package]
WHERE (CHARINDEX(@Filter, Id) > 0 OR CHARINDEX(@Filter, PackageType) > 0 OR CHARINDEX(@Filter, Contents) > 0 OR @Filter IS NULL OR LEN(@Filter) = 0) 
END

