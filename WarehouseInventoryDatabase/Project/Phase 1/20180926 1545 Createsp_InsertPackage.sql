﻿CREATE PROCEDURE [dbo].[spInsertPackage]
    @Id UNIQUEIDENTIFIER,
	@PackageType VARCHAR(100),
	@Weight FLOAT,
	@Length FLOAT,
	@Height FLOAT,
	@Width FLOAT,
	@Quantity INT,
	@Contents VARCHAR(100),
	@IsFragile BIT
AS
	BEGIN
		INSERT INTO Package 
		Values(@Id,@PackageType, @Weight, @Length, @Height, @Width, @Quantity, @Contents, @IsFragile)
	END

