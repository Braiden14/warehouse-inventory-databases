﻿CREATE PROCEDURE [dbo].[spUpdatePackage]
	@Id UNIQUEIDENTIFIER,
	@PackageType VARCHAR(100),
	@Weight FLOAT,
	@Length FLOAT,
	@Height FLOAT,
	@Width FLOAT,
	@Quantity INT,
	@Contents VARCHAR(100),
	@IsFragile BIT
AS
	BEGIN
		UPDATE Package
		SET [PackageType] = @PackageType,
			[Weight] = @Weight,
			[Length] = @Length,
			[Height] = @Height,
			[Width] = @Width,
			[Quantity] = @Quantity, 
			[Contents] = @Contents,
			[IsFragile] = IsFragile
		WHERE Id = @Id
	END
