﻿CREATE PROCEDURE spGetPackageById
@Id UNIQUEIDENTIFIER
AS
BEGIN 
SELECT [Id]
      ,[PackageType]
      ,[Weight]
      ,[Length]
      ,[Height]
      ,[Width]
      ,[Quantity]
      ,[Contents]
      ,[IsFragile]
  FROM [dbo].[Package]
WHERE Id = @Id
END